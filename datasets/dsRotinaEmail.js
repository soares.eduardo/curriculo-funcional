function defineStructure() {
    var dataset = DatasetBuilder.newDataset();
    dataset.addColumn("RetornoEnvioEmail"); //coluna com o retorno do try-catcha
}
function onSync(lastSyncDate) {

    var meta = DatasetFactory.createConstraint("metadata#active", true, true, ConstraintType.MUST);
    var result = DatasetFactory.getDataset("dsFormCurriculoFuncionalEduardo", null, [meta], null);

    for (var index = 0; index < result.rowsCount; index++) {
        var nome = result.getValue(index, "inputNome");
        var data = result.getValue(index, "inputData");
        var email = DatasetFactory.createConstraint("colleagueName", nome, nome, ConstraintType.MUST);
        var ret = DatasetFactory.getDataset("colleague", null, [email], null);
        var mail = ret.getValue(0, "mail");

        verifyDate(data, mail);
    }

    function verifyDate(data, mail) {
        var dataAtual = new Date().toLocaleDateString('pt-BR');
        var dataCadastro = new Date(data).toLocaleDateString('pt-BR');
        var dia = dataCadastro.split('/')[0];
        var mes = parseInt(dataCadastro.split('/')[1]);
        var ano = parseInt(dataCadastro.split('/')[2]);
        
        // 6 meses
        if (mes + 6 >= 12) {
            mes = mes + 6;
            mes = mes - 12 == 0 ? 12 : mes - 12;
            ano = ano + 1;

            mostraMes = mes < 10 ? "0" + mes : mes;
            mostraMes =  mes == 0 ? 1 : "0" + mes;
            var dataTotal = dia + "/" + mostraMes + "/" + ano;
            
            if (dataAtual == dataTotal) {
                enviaEmail(mail);
            }
        } else {
            mes = mes + 6;
            mostraMes = mes < 10 ? "0" + mes : mes;
            var dataTotal = dia + "/" + mostraMes + "/" + ano;
            
            if (dataAtual == dataTotal) {
                enviaEmail(mail);
            }
        }

        //12 meses
        if (mes + 12 >= 12) {
            mes = mes + 12;
            mes = mes - 12 == 0 ? 12 : mes - 12;
            ano = ano + 1;

            mostraMes = mes < 10 ? "0" + mes : mes;
            mostraMes = mes == 0 ? 1 : mes;
            var dataTotal = dia + "/" + mostraMes + "/" + ano;
            
            if (dataAtual == dataTotal) {
                enviaEmail(mail);
            }
        } else {
            mes = mes + 12;
            mostraMes = mes < 10 ? "0" + mes : mes;
            var dataTotal = dia + "/" + mostraMes + "/" + ano;
            
            if (dataAtual == dataTotal) {
                enviaEmail(mail);
            }
        }
    }

    function enviaEmail(user) {
        try {

            var obj = new com.fluig.foundation.mail.service.EMailServiceBean(); //Serviço do Fluig

            //usuário que receberá o e-mail
            var destinatario = user;
            var assunto = "Atualize seu currículo funcional"; //assunto
            var emailFluig = "fluig.parus@totvsrs.com.br"; //quem enviou o e-mail
            var corpoEmail = "Seu currículo funcional está perto de expirar. Realize a atualização."; //corpo do e-mail
            obj.simpleEmail(1, assunto, emailFluig, destinatario, corpoEmail, "text/html");

            //Executou o envio do e-mail
            dataset.addRow(new Array("OK"));

        } catch (e) {
            log.info("============== Erro e-mail envio de e-mail Solicitante Service Desk: " + e);
            //Executou com erro o envio
            dataset.addRow(new Array("Erro"));
        }

    }

    return dataset;
}
function createDataset(fields, constraints, sortFields) {

} function onMobileSync(user) {

}