function createDataset(fields, constraints, sortFields) {

    var ds = DatasetBuilder.newDataset();

    ds.addColumn("modulo");
    ds.addColumn("produto");

    //Fluig

    ds.addRow(new Array("BPM",1));
    ds.addRow(new Array("WCM",1));
    ds.addRow(new Array("LMS",1));

    //RM

    ds.addRow(new Array("RM BIS",3));
    ds.addRow(new Array("RM AGILIS",3));
    ds.addRow(new Array("RM BIBLIOS",3));

    //Protheus

    ds.addRow(new Array("SIGAATF",2));
    ds.addRow(new Array("SIGACOM",2));
    ds.addRow(new Array("SIGAEST",2));

    }
    