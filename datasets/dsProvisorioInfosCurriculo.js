function createDataset(fields, constraints, sortFields) {

    var ds = DatasetBuilder.newDataset();

    ds.addColumn("Nome");
    ds.addColumn("Matrícula");
    ds.addColumn("Data");
    ds.addColumn("Admissão");
    ds.addColumn("Cargo");

    ds.addRow(new Array("Eduardo A. Soares", "41231231", "04/04/2018", "05/02/1995", "RH"));
    ds.addRow(new Array("Gabriel A. Melo", "112312313", "05/10/2015", "11/12/1984", "Desenvolvedor"));
    ds.addRow(new Array("Douglas Marques", "22542109", "15/03/2004", "07/02/1980", "Analista"));
    ds.addRow(new Array("Otavião Bastilhos", "3231231313", "31/12/2005", "54/03/1978", "Coordenador"));
    ds.addRow(new Array("Lucas Silveira Bernardo", "66666666", "01/06/2010", "30/04/2000", "Coordenador"));

    return ds;

}