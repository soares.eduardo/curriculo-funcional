function createDataset(fields, constraints, sortFields) {

    var ds = DatasetBuilder.newDataset();
    
    ds.addColumn("id");
    ds.addColumn("Produto");
    
    ds.addRow(new Array(1, "Fluig"));
    ds.addRow(new Array(2, "Protheus"));
    ds.addRow(new Array(3, "RM"));
    
    return ds;
}
   