function validateForm(form) {

    var msg = "";
    var estado = getValue("WKNumState");

    //IDENTIFICAÇÃO DO TOTVER

    if (form.getValue("inputNome") == null) {
        msg += "Campo nome não foi preenchido.\n";
    }

    //CONHECIMENTO DA SOLUÇÕES

    if (form.getValue("inputConhecimentoSolucao") == "") {
        msg += "Campo de conhecimento nas soluções não foi preenchido.\n";
    }

    //FORMAÇÃO

    var indexFormacao = form.getChildrenIndexes("formacaoPaiFilho");

    if (indexFormacao.length < 1) {
        msg += "Nenhum campo de formação cadastrado.\n"
    } else {
        for (var i = 0; i < indexFormacao.length; i++) {
            if (form.getValue("inputCursoPaiFilho___" + indexFormacao[i]) == "") {
                msg += "Campo curso não preenchido.\n";
            }
            if (form.getValue("inputInstituicaoPaiFilho___" + indexFormacao[i]) == "") {
                msg += "Campo instituição não preenchido.\n";
            }
            if (form.getValue("inputStatusPaiFilho___" + indexFormacao[i]) == "") {
                msg += "Campo  status não preenchido.\n";
            }
        }
    }

    //EXPERIENCIA NA FUNÇÃO

    var indexExperienciaFuncao = form.getChildrenIndexes("experienciaNaFuncao");

    if(indexExperienciaFuncao.length < 1){
        msg += "Nenhum campo de experiencia na função cadastrado.\n";
    }else{
        for(var i = 0; i < indexExperienciaFuncao.length; i++){
            if(form.getValue("inputFuncoesPaiFilho___" + indexExperienciaFuncao[i]) == ""){
                msg += "Campo função não preenchido.\n";
            }
        }
    }

    //EXPERIENCIA EM PROCESSO

    var indexExperienciaProcesso = form.getChildrenIndexes("processosPaiFilho");

    if(indexExperienciaProcesso < 1){
        msg += "Nenhum campo de experiencia de processos cadastrado.\n";
    }else{
        for(var i = 0; i < indexExperienciaProcesso.length; i++){
            if(form.getValue("inputClientePaiFilho___" + indexExperienciaProcesso[i]) == ""){
                msg += "Campo cliente não preenchido.\n";
            }
            if(form.getValue("inputProjetoPaiFilho___" + indexExperienciaProcesso[i]) == ""){
                msg += "Campo projeto não preenchido.\n";
            }
            if(form.getValue("inputFuncaoPaiFilho___" + indexExperienciaProcesso[i]) == ""){
                msg += "Campo função não preenchido.\n";
            }
            if(form.getValue("inputInicioPaiFilho___" + indexExperienciaProcesso[i]) == ""){
                msg += "Campo inico não preenchido.\n";
            }
            if(form.getValue("inputFimPaiFilho___" + indexExperienciaProcesso[i]) == ""){
                msg += "Campo fim não preenchido.\n";
            }
        }
    }

    if(msg != ""){
        throw msg;
    }
}