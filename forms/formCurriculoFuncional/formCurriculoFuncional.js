function setSelectedZoomItem(selectedItem) {
    var posicao = selectedItem.inputId.split('___')[1];

    if (selectedItem.inputName == "inputProduto___" + posicao) {
        $('#inputModulo___' + posicao).attr('disabled', false);
        reloadZoomFilterValues("inputModulo___" + posicao, "inputProduto," + selectedItem.id);
    }

    if (selectedItem.inputName == "inputClientePaiFilho___" + posicao) {
        $('#inputProjetoPaiFilho___' + posicao).attr('disabled', false);
        reloadZoomFilterValues("inputProjetoPaiFilho___" + posicao, "nom_sims," + selectedItem.A1_COD);
    }
}

function removedZoomItem(removedItem) { //Remove in case the selected has been unsetted
    if (removedItem.inputName == "inputNome") {
        document.querySelector('#inputTotver').value = '';
        document.querySelector('#inputMatricula').value = '';
        // document.querySelector('#inputData').value = '';
        document.querySelector('#inputAdmissao').value = '';
        document.querySelector('#inputCargo').value = '';
        document.querySelector('#inputNome').value = '';
    }

    var posicao = removedItem.inputId.split('___')[1];

    if (removedItem.inputName == "inputProduto___" + posicao) {
        $('#inputModulo___' + posicao).text('');
        $('#inputModulo___' + posicao).attr('disabled', true);
    }

    if (removedItem.inputName == "inputClientePaiFilho___" + posicao) {
        $('#inputProjetoPaiFilho___' + posicao).text('');
        $('#inputProjetoPaiFilho___' + posicao).attr('disabled', true);
    }
}

function getCurrentDate() {

    var currentDate = new Date();

    var dia = currentDate.getDate();
    var mes = currentDate.getMonth() + 1;
    var ano = currentDate.getFullYear();

    if (dia < 10) {
        dia = '0' + dia;
    }
    if (mes < 10) {
        mes = '0' + mes;
    }

    currentDate = dia + "/" + mes + "/" + ano;

    return currentDate;
}


$(document).ready(function () {

    var user = parent.WCMAPI.getUser();
    $('#inputNome').val(user);

    $('#inputNome').change(function(){
        if($(this).val() != null && $(this).val() !="") {
            var campoDescritor = $('#inputNome').val() +  " " + getCurrentDate();
            document.querySelector('#campoDescritor').value = campoDescritor; //Add to somewhere else
        }
    })

    function geraWord() {

        function loadFile(url, callback) {
            JSZipUtils.getBinaryContent(url, callback);
        }

        loadFile("model4.docx", function (error, content) {
            if (error) {
                throw error
            };
            var zip = new JSZip(content);
            var doc = new Docxtemplater().loadZip(zip)

            //Campos que irão ser passados para o word  
            var inputNome = $("#inputNome").val();
            var inputMatricula = $("#inputMatricula").val();
            var inputData = $("#inputData").val();
            var inputAdmissao = $("#inputAdmissao").val();
            var inputCargo = $("#inputCargo").val();
            var inputDRG = $("#inputDRG").val();
            var inputRevisao = $("#inputRevisao").val();
            var inputConhecimentoSolucao = $("#inputConhecimentoSolucao").val();
            var numb = "Currículo Funcional-" + inputNome;

            var formacao = "";
            for (var i = 0; i < $('[id^=inputCursoPaiFilho___]').length; i++) {
                var ied = $('[id^=inputCursoPaiFilho___]')[i].id;
                var index = ied.substring(21, 23);

                var cur = $("#inputCursoPaiFilho___" + index).val();
                var ins = $("#inputInstituicaoPaiFilho___" + index).val();
                var sta = $("#inputStatusPaiFilho___" + index).val();

                formacao = formacao + '{"cur":"' + cur + '","ins":"' + ins + '","sta":"' + sta + '"},';
            }

            formacao = "[" + formacao.replace(/,$/, "]")

            var expFuncao = "";
            for (var i = 0; i < $('[id^=inputProduto___]').length; i++) {
                var ied = $('[id^=inputProduto___]')[i].id;
                var index = ied.substring(15, 17);

                var produto = $("#inputProduto___" + index).val();
                var modulo = $("#inputModulo___" + index).val();
                var nivel = $("#inputNivelExperiencia___" + index).val();

                expFuncao = expFuncao + '{"produto":"' + produto + '", "modulo":"' + modulo + '", "nivel":"' + nivel + '"},';
            }

            expFuncao = "[" + expFuncao.replace(/,$/, "]")

            var expProcessos = "";
            for (var i = 0; i < $('[id^=inputClientePaiFilho___]').length; i++) {
                var ied = $('[id^=inputClientePaiFilho___]')[i].id;
                var index = ied.substring(23, 25);

                var cliente = $("#inputClientePaiFilho___" + index).val();
                var projeto = $("#inputProjetoPaiFilho___" + index).val();
               
                expProcessos = expProcessos + '{"cliente":"' + cliente + '","projeto":"' + projeto + '"},';
            }

            expProcessos = "[" + expProcessos.replace(/,$/, "]")

            //Numb
            var identif = numb;
            //colocando dentro das variaveis do word
            doc.setData({
                "inputNome": inputNome,
                "inputMatricula": inputMatricula,
                "inputData": inputData,
                "inputAdmissao": inputAdmissao,
                "inputCargo": inputCargo,
                "inputDRG": inputDRG,
                "inputRevisao": inputRevisao,
                "curso": JSON.parse(formacao),
                "expFun": JSON.parse(expFuncao),
                "exp": JSON.parse(expProcessos),
                "inputConhecimentoSolucao": inputConhecimentoSolucao
            });

            try {
                // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
                doc.render()
            } catch (error) {
                var e = {
                    message: error.message,
                    name: error.name,
                    stack: error.stack,
                    properties: error.properties,
                }
                console.log(JSON.stringify({
                    error: e
                }));
                // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
                throw error;
            }

            var out = doc.getZip().generate({
                type: "blob",
                mimeType: "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            }) //Output the document using Data-URI
            saveAs(out, identif + ".docx")
        })
    };

    $("#btn-anexar").click(function () {
        parent.frames.document.getElementById("ecm-navigation-inputFile-clone").click();
    });

    $('#adicionarFormacao').click(function () {
        var add = wdkAddChild('formacaoPaiFilho');
    });

    $('#adicionarExp').click(function () {
        var add = wdkAddChild('experienciaNaFuncao');
        $('#inputModulo___'+add).attr('disabled', true);
    });

    $('#adicionarExpProcessos').click(function () {
        var add = wdkAddChild('processosPaiFilho');
        $('#inputProjetoPaiFilho___'+add).attr('disabled', true);
    });

    var estado = getWKNumState();

    if (estado == 5) {
        $('#gerarRelatorio').show();
        $('#adicionarExp').hide();
        $('.form-control').each(function (i) {
            $(this).attr('readonly', true);
        });
        $('.fluigicon-trash ').hide();
        $('.table').find('tbody').find(':button').hide();
    }

    $('#inputData').val(getCurrentDate());

    $('#gerarRelatorio').click(function () {
        geraWord();
    });
});
